package com.kakaopay.task;

import java.util.Arrays;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.kakaopay.task.util.UStringTask;

/**
 * @author       정범훈
 * @since        2019.01.31
 * @description  String TEST
 * @history
 *       
 *  수정일                    수정자                  수정내용
 *  ----------   ---------   -------------------------------
 *  2019.01.31      정범훈                  최초생성	
 *****************************************************************************************************/
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class StringTest {

	@Test
	public void t1_find() {

		List<String> titles = Arrays.asList("집안일", "빨래 @1", "청소 @1", "방청소 @1 @3");		
		for(String s : titles) { getRefenceSeq(s); }
		for(String s : titles) { UStringTask.getRefenceSeq(s); }
	}
	
	private int[] getRefenceSeq(String title) {

		// @ 카운팅
		int countMentions = 0;
		char titles[] = title.toCharArray();
		for(int i = 0; i < titles.length; i++) {
			if (titles[i] == '@') {
				countMentions++; 
			}
		}
		
		// referenceSeqs
		int[] referenceSeqs = new int[countMentions];
		String splitTitle[] = title.split(" ");
		
		int i = 0;
		System.out.println("=== title : " + title);
		for(String s : splitTitle) {
			if(0 == s.indexOf('@') && s.length() > 0) {
				referenceSeqs[i] = Integer.parseInt(s.substring(1));
				System.out.print(" [" + referenceSeqs[i] + "] ");
				i++;
			}
		}
		System.out.println("");
		
		return referenceSeqs;
	}
}
