package com.kakaopay.task;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.kakaopay.common.exception.UnAuthorizedException;
import com.kakaopay.task.domain.Task;
import com.kakaopay.task.domain.TaskForm;
import com.kakaopay.task.domain.TaskForm.UpdateForm;
import com.kakaopay.task.service.TaskReferenceService;
import com.kakaopay.task.service.TaskService;

/**
 * @author       정범훈
 * @since        2019.01.31
 * @description  SERVICE TEST
 * @history
 *       
 *  수정일                    수정자                  수정내용
 *  ----------   ---------   -------------------------------
 *  2019.01.31      정범훈                  최초생성	
 *****************************************************************************************************/
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Transactional
public class TaskTest {

	@Autowired TaskService          taskService;
	@Autowired TaskReferenceService taskReferenceService;
	@Autowired MockHttpSession      session;
	
	@Before
	public void setup() {
		session = new MockHttpSession();
		session.setAttribute("title", "집안일");
//		session.setAttribute("title", "빨래 @1");
//		session.setAttribute("title", "청소 @1");
//		session.setAttribute("title", "방청소 @1 @3");
	}

	@Test
	@Transactional
	public void t1_insert() {

		/* Given 
		 **************************************************************/
//		String title = String.valueOf(session.getAttribute("title"));
		String title = "집안일 t1";

		/* When : 정상입력
		 **************************************************************/
		TaskForm.InsertForm task = new TaskForm.InsertForm();
		task.setTitle(title);
		taskService.insert(task);
	}

	@Test
	@Transactional
	public void t2_select() {

		/* Given 
		 **************************************************************/
		String title = "빨래 t2 @1";

		/* When : 입력 후 마지막 할일 조회
		 **************************************************************/
		TaskForm.InsertForm task = new TaskForm.InsertForm();
		task.setTitle(title);
		taskService.insert(task);
		Task findTask = taskService.selectLastTask();

		/* Then : 정상입력 확인, 입력한 할일과 저장된 할일이 동일 한지 확인
		 **************************************************************/
		assertNotNull(findTask);
		assertEquals (findTask.getTitle(), title);
	}

	@Test
	@Transactional
	public void t3_update() {

		/* Given 
		 **************************************************************/
		String title = "청소 t3";

		/* When : 입력 후 마지막 할일 가져와서 수정
		 **************************************************************/
		TaskForm.InsertForm task = new TaskForm.InsertForm();
		task.setTitle(title);
		Task savedTask = taskService.insert(task);

		String changedTitle = "청소 t3 수정";
		
		TaskForm.UpdateForm updateForm = new UpdateForm();
		updateForm.setTitle(changedTitle);
		Task updatedTask = taskService.update(updateForm, savedTask.getSeq());

		/* Then : 수정한 할일과 저장된 할일이 동일 한지 확인
		 **************************************************************/
		assertEquals (updatedTask.getTitle(), changedTitle);
	}

	/**
	 * throw new UnAuthorizedException(); 에서 먼저 걸려서 delete 가 안 됨... > 데이터가 남아있음
	 * @Transactional 을 걸면 에러
	 */
//	@Test(expected=UnAuthorizedException.class)
	@Transactional
	public void t4_complete_fail() {

		/* Given 
		 **************************************************************/
		String title = "방청소 t4";

		/* When : 입력 후 참조된 항목 추가 입력 후 완료처리 시도
		 **************************************************************/
		TaskForm.InsertForm task1 = new TaskForm.InsertForm();
		task1.setTitle(title);
		Task savedTask1 = taskService.insert(task1);
		
		String title2 = title + " @" + String.valueOf(savedTask1.getSeq());
		
		TaskForm.InsertForm task2 = new TaskForm.InsertForm();
		task2.setTitle(title2);
		Task savedTask2 = taskService.insert(task2);
		
		taskService.complete(savedTask2.getSeq());

		// throw new UnAuthorizedException(); > 데이터가 남아있음
		// @Transactional 을 걸면 에러
		System.out.println("=== savedTask1.getSeq() : " + savedTask1.getSeq());
		System.out.println("=== savedTask2.getSeq() : " + savedTask2.getSeq());

		taskService.delete(savedTask1.getSeq());
		taskService.delete(savedTask2.getSeq());
	}

	@Test
	public void t5_complete() {

		/* Given 
		 **************************************************************/
		String title = "방청소 t5";

		/* When : 입력 후 참조된 항목 추가 입력 후 완료처리 시도
		 **************************************************************/
		TaskForm.InsertForm task1 = new TaskForm.InsertForm();
		task1.setTitle(title);
		Task savedTask1 = taskService.insert(task1);
		
		String title2 = title + " @" + String.valueOf(savedTask1.getSeq());
		
		TaskForm.InsertForm task2 = new TaskForm.InsertForm();
		task2.setTitle(title2);
		Task savedTask2 = taskService.insert(task2);

		Task completetedTask1 = taskService.complete(savedTask1.getSeq());
		Task completetedTask2 = taskService.complete(savedTask2.getSeq());

		/* Then : 모두 완료
		 **************************************************************/
		assertEquals(true, completetedTask1.isComplete());
		assertEquals(true, completetedTask2.isComplete());
	}
}
