<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="container">
  <!-- table -->
  <h2>할일</h2>
  <p>* 할일을 조회 합니다. </p>
  <form class="form-inline" action="" method="post" id="listFrm">
	<div class="form-group hidden-xs">
	<div class="form-group">
	<input type="text"   id="title" />
	<input type="hidden" id="seq" />
	<span class="rManager rFinance rDirectors">
	  <a href="javascript:f_save();" class="btn btn-primary">저장</a>
	</span>
	</div>
  </form>
  <div class="">
  <table class="table table-bordered" style="margin-top:10px">
	<colgroup>
	  <col width="10%" />
	  <col width="40%" />
	  <col width="20%" />
	  <col width="20%" />
	  <col width="10%" />
	</colgroup>
    <thead>
      <tr class="active">
        <th scope="col">id</th>
        <th scope="col">할일</th>
        <th scope="col">작성일시</th>
        <th scope="col">최종수정일시</th>
        <th scope="col">완료처리</th>
      </tr>
    </thead>
    <tbody>
      <c:forEach items="${list.content}" var="list" varStatus="status">
      	<tr>
	        <td><c:out value="${list.seq}" /></td>
	        <td><a href="#listFrm" onclick="javascript:f_modify_move('<c:out value="${list.seq}" />', '<c:out value="${list.title}" />');"><c:out value="${list.title}" /></a></td>
	        <td><fmt:formatDate value="${list.createDate}" pattern="YYYY-MM-dd HH:mm:ss"/></td>
	        <td><fmt:formatDate value="${list.updateDate}" pattern="YYYY-MM-dd HH:mm:ss"/></td>
	        <td id="complete<c:out value="${list.seq}" />">
	          <c:choose>
	        	<c:when test="${true eq list.complete}">
	        	  완료됨
	        	</c:when>
	        	<c:otherwise>
	        	 <button onclick="javascript:f_complete(<c:out value="${list.seq}" />);">완료처리</button>
	        	</c:otherwise>
	          </c:choose>
	        </td>
	      </tr>
      </c:forEach>
      <c:if test="${empty list}"><tr><td colspan="5">데이터가 없습니다.</td></tr></c:if>
    </tbody>
  </table>
  </div>
  <!-- pagebar -->
  <c:import url="/WEB-INF/jsp/task/pagebar.jsp" charEncoding="UTF-8">
	<c:param name="number" value="${list.number}" />
	<c:param name="totalPages" value="${list.totalPages}" />
	<c:param name="size" value="${list.size}" />
  </c:import>
</div>
<script type="text/javascript">

// 수정 폼
function f_modify_move(seq, title) {
	$("#seq"  ).val(seq);
	$("#title").val(title);
}

// 저장하기
function f_save() {
	var seq = $("#seq").val();
	if("" == seq) {
		f_insert();
	} else {
		f_update(seq);
	}
}

// 등록
function f_insert() {
	
	var params = {
		title : $("#title").val()
	};
	
	$.ajax({
      type     : "POST",
      url      : "/api/tasks",
      dataType : "json",
	  contentType : "application/json; charset=utf-8",
	  data : JSON.stringify(params),
      success  : function(){
       	 alert( "등록 되었습니다." );
       	 location.href = "/task"; 
      },
      error    : function(e){ alert(e); }
  });
}

// 수정
function f_update(seq) {
	
	var params = {
		seq   : seq,
		title : $("#title").val()
	};
	
	$.ajax({
      type     : "PUT",
      url      : "/api/tasks/" + seq,
      dataType : "json",
	  contentType : "application/json; charset=utf-8",
	  data : JSON.stringify(params),
      success  : function(){
       	 alert( "수정 되었습니다." );
       	 location.href = "/task"; 
      },
      error    : function(e){ alert(e); }
  });
}

// 완료처리
function f_complete(seq) {
	
	var url   = "/api/tasks/"+seq+"/complete";
	
	$.ajax({
      type     : "GET",
      url      : url,
      dataType : "json",
      data     : "",
      success  : function(){
       	 alert( "완료처리 되었습니다." );
       	 location.href = "/task"; 
      },
      error    : function (XMLHttpRequest, textStatus, errorThrown) {
		switch (XMLHttpRequest.status) {
			case 401:
				alert( "참조된 완료되지 않은 할일이 있습니다." );
				location.href = "/task";
    	  }
      }
  });
}
</script>