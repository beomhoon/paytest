<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<c:set var="setParam" value="${param.setParam}" />
<c:set var="pageNumber" value="${param.number}" />
<c:set var="maxPages" value="${param.totalPages}" />
<c:set var="size" value="${param.size}" />
<c:set var="pageBlock" value="5" />
<fmt:formatNumber value="${((pageNumber+1)/pageBlock)+(1-(((pageNumber+1)/pageBlock)%1))%1}" type="number" var="startNumber"/>
<div class="span12">
<ul class="pager text-right">
	<c:if test="${pageNumber gt 0}">
		<spring:url value="" var="first">
			<spring:param name="page" value="0" />
			<spring:param name="size" value="${size}" />
		</spring:url>
		<spring:url value="" var="previous">
			<spring:param name="page" value="${pageNumber - 1}" />
			<spring:param name="size" value="${size}" />
		</spring:url>
		<spring:message code="list_first" var="first_label" text="First" htmlEscape="false" />
		<spring:message code="list_previous" var="previous_label" text="Previous" htmlEscape="false"/>
		<li><a href="${first}${setParam}" title="${fn:escapeXml(first_label)}">&lt;&lt;</a></li>
		<li><a href="${previous}${setParam}" title="${fn:escapeXml(previous_label)}">&lt;</a></li>
	</c:if>&nbsp;
	<c:forEach begin="${((startNumber)*pageBlock)-4}" end="${(startNumber)*pageBlock}" varStatus="status">
		<c:if test="${status.index le maxPages}">
			<spring:url value="" var="num">
				<spring:param name="page" value="${status.index-1}" />
				<spring:param name="size" value="${size}" />
			</spring:url>
			<li><a href="${num}${setParam}" title="Page {<c:out value="${status.index}" />}" <c:if test="${(status.index-1) eq pageNumber}">class="active"</c:if>><c:out value="${status.index}" /></a></li>
		</c:if>
	</c:forEach>&nbsp;
	<c:if test="${pageNumber lt (maxPages-1)}">
		<spring:url value="" var="next">
			<spring:param name="page" value="${pageNumber + 1}" />
			<spring:param name="size" value="${size}" />
		</spring:url>
		<spring:url value="" var="last">
			<spring:param name="page" value="${maxPages-1}" />
			<spring:param name="size" value="${size}" />
		</spring:url>
		<spring:message code="list_last" var="last_label" text="Last"
			htmlEscape="false" />

		<spring:message code="list_next" var="next_label" text="Next"
			htmlEscape="false" />
		<li><a href="${next}${setParam}" title="${fn:escapeXml(next_label)}">&gt;</a></li>
		<li><a href="${last}${setParam}" title="${fn:escapeXml(last_label)}">&gt;&gt;</a></li>
	</c:if>
</ul>
</div>