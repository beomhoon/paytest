package com.kakaopay.common.exception.common;

/**
 * @author jbeomh@gmail.com
 * @date 2017. 9. 19.
 *
 */
public enum ExceptionCode {

	/**
	 * E0001xxxx : 공통
	 ********************************************************************************************/
	 E00010001 // 유효하지 않은 데이터 입니다.
	,E00010002 // 해당 정보가 없습니다.
	,E00010003 // 참조된 완료되지 않은 할일이 있습니다.
}
