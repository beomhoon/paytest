package com.kakaopay.common.exception;

import com.kakaopay.common.exception.common.ExceptionCode;
import com.kakaopay.common.exception.common.ExceptionResponse;
import com.kakaopay.common.helper.message.MessageHelper;

import lombok.Getter;
import lombok.Setter;

/**
 * @author jbeomh@gmail.com
 * @date 2017. 9. 19.
 * @description  일반적인 어플리케이션 예외(어플리케이션 예외 최상위)
 *
 */
@Setter
@Getter
@SuppressWarnings("serial")
public class SystemException extends RuntimeException {
 
	/**
	 *      기본 예외 코드 설정(E00010001)
	 * <br> 일반적인 어플리케이션 예외(어플리케이션 예외 최상위)
	 * <br><br>
	 ********************************************************************************************/
	public SystemException(){

		exceptionResponse.setCode   (ExceptionCode.E00010001);
		exceptionResponse.setMessage(MessageHelper.getMessage(ExceptionCode.E00010001)); 
	}
	
	/**
	 *      지정 예외 코드 설정
	 * <br> 일반적인 어플리케이션 예외(어플리케이션 예외 최상위)
	 * <br><br>
	 * 
	 * @param exceptionCode 예외 코드
	 ********************************************************************************************/
	public SystemException(ExceptionCode exceptionCode){

		exceptionResponse.setCode   (exceptionCode);
		exceptionResponse.setMessage(MessageHelper.getMessage(exceptionCode)); 
	}
	
	
	private ExceptionResponse exceptionResponse = new ExceptionResponse();
}



