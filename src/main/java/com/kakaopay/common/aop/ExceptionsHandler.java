package com.kakaopay.common.aop;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.kakaopay.common.exception.NoDataException;
import com.kakaopay.common.exception.SystemException;
import com.kakaopay.common.exception.UnAuthorizedException;;

/**
 * @author jbeomh@gmail.com
 * @date 2017. 9. 19.
 * reference site : http://springboot.tistory.com/33
 *  response code : http://onecellboy.tistory.com/346
 */
@ControllerAdvice
public class ExceptionsHandler {

    /**
     * 일반적인 어플리케이션 예외(어플리케이션 예외 최상위)
     * @return 응답 정보(response status code : 400 잘못된요청)
     */
	@ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = SystemException.class)
    public ResponseEntity<?> handleSystemException(HttpServletRequest request, SystemException exception) {
		return new ResponseEntity<>(exception.getExceptionResponse(), HttpStatus.BAD_REQUEST);
	}
	
    /**
     * ExceptionCode : E00010002
     * @return 응답 정보(response status code : 204 컨텐츠 없음)
     */
	@ResponseStatus(HttpStatus.NO_CONTENT)
    @ExceptionHandler(value = NoDataException.class)
    public ResponseEntity<?> handleNoDataException(HttpServletRequest request, NoDataException exception) { 
		return new ResponseEntity<>(exception.getExceptionResponse(), HttpStatus.NO_CONTENT);
	}
    
    /**
     * ExceptionCode : E00010003
     * @return 응답 정보(response status code : 401 권한 없음)
     */
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(value = UnAuthorizedException.class)
    public ResponseEntity<?> handleUnAuthorizedException(HttpServletRequest request, UnAuthorizedException exception) { 
		return new ResponseEntity<>(exception.getExceptionResponse(), HttpStatus.UNAUTHORIZED);
	}
}
