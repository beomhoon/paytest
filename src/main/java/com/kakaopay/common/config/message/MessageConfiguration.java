package com.kakaopay.common.config.message;

import java.util.Locale;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.google.common.base.Charsets;
 
/**
 * @author jbeomh@gmail.com
 * @date 2017. 9. 19.
 * @description  MESSAGE CONFIG
 *
 */
@Configuration
public class MessageConfiguration {

    @Bean
    public MessageSourceAccessor messageSourceAccessor(){
    	
    	ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("/config/message/messages");
		messageSource.setDefaultEncoding(Charsets.UTF_8.name());
		
    	return new MessageSourceAccessor(messageSource); 
    }
    
    @Bean
    public LocaleResolver localeResolver() {
        
    	SessionLocaleResolver sessionLocaleResolver = new SessionLocaleResolver();
        sessionLocaleResolver.setDefaultLocale(Locale.KOREA);
        
        return sessionLocaleResolver;
    }
}