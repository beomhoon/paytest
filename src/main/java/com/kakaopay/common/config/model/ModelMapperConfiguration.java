package com.kakaopay.common.config.model;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
 
/**
 * @author jbeomh@gmail.com
 * @date 2017. 9. 17.
 * @description  MODEL MAPPER CONFIG
 *
 */
@Configuration
public class ModelMapperConfiguration {

	@Bean
	public ModelMapper modelMapper(){
		
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper;
	}
}