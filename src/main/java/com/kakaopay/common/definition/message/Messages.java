package com.kakaopay.common.definition.message;

/**
 * @author jbeomh@gmail.com
 * @date 2017. 8. 29.
 *
 */
public enum Messages {
 
	/**
	 * E0001xxxx : 공통
	 ********************************************************************************************/
	 E00010001  // 유효성 검사에 실패하였습니다.
	,E00010002  // 해당 정보가 없습니다.
	,E00010003  // 잘못된 권한으로 요청하였습니다.
}
