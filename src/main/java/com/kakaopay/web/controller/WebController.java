package com.kakaopay.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kakaopay.common.helper.model.ModelHelper;
import com.kakaopay.task.domain.TaskForm;
import com.kakaopay.task.service.TaskService;

/**
 * @author       정범훈
 * @since        2019.01.31
 * @description  CONTROLLER
 * @history
 *       
 *  수정일                    수정자                  수정내용
 *  ----------   ---------   -------------------------------
 *  2019.01.31      정범훈                  최초생성	
 *****************************************************************************************************/
@Controller
public class WebController {

	/**
	 * 목록 페이지 이동
	 * <br><br>
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/task", method= RequestMethod.GET)
	public String selectTaskList(Pageable pageable, Model model) {
		
		model.addAttribute("list", ModelHelper.page(taskService.selectList(pageable), TaskForm.ResponseForm.class, pageable));
		return "/task/list";
	}
	
	@Autowired private TaskService taskService = null;
}
