package com.kakaopay.task.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kakaopay.common.exception.NoDataException;
import com.kakaopay.task.domain.TaskReference;
import com.kakaopay.task.repository.TaskReferenceRepository;

/**
 * @author       정범훈
 * @since        2019.01.31
 * @description  SERVICE
 * @history
 *       
 *  수정일                    수정자                  수정내용
 *  ----------   ---------   -------------------------------
 *  2019.01.31      정범훈                  최초생성	
 *****************************************************************************************************/
@Service
@Transactional
public class TaskReferenceService {
	
	/** 
	 * 할일 참조  목록
	 * <br><br>
	 * 
	 * @return          할일 참조  목록
	 */
	@Transactional(readOnly = true)
	public List<TaskReference> selectList(Long seq){
		
		List<TaskReference> list = referenceTaskRepository.findAllBySeq(seq);
		if(null == list) { throw new NoDataException(); }
		
		return list;
	}
	
	/**
	 * 할일 참조  등록 
	 * <br><br>
	 * 
	 * @param  TaskReference 할일 참조  요청 정보
	 * @return      할일 참조  등록
	 */
	public TaskReference insert(Long seq, Long refSeq) {
		
		TaskReference referenceTask = new TaskReference(seq, refSeq);
		return referenceTaskRepository.save(referenceTask);
	}
	
	/** 
	 * 할일 참조  삭제
	 * <br><br>
	 * 
	 * @param  seq 할일 참조  아이디
	 * @return     할일 참조  삭제
	 */
	public void delete(Long seq) {

		List<TaskReference> list = referenceTaskRepository.findAllBySeq(seq);

		if(null != list && list.size() > 0) { 
			referenceTaskRepository.deleteById(seq);
		}
	}
	
	@Autowired private TaskReferenceRepository referenceTaskRepository = null;
}
