package com.kakaopay.task.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kakaopay.common.exception.NoDataException;
import com.kakaopay.common.exception.UnAuthorizedException;
import com.kakaopay.task.domain.Task;
import com.kakaopay.task.domain.TaskForm;
import com.kakaopay.task.domain.TaskReference;
import com.kakaopay.task.repository.TaskRepository;
import com.kakaopay.task.util.UStringTask;

/**
 * @author       정범훈
 * @since        2019.01.31
 * @description  SERVICE
 * @history
 *       
 *  수정일                    수정자                  수정내용
 *  ----------   ---------   -------------------------------
 *  2019.01.31      정범훈                  최초생성	
 *****************************************************************************************************/
@Service
@Transactional
public class TaskService {
	
	/**
	 * 완료 처리
	 * <br><br>
	 * 
	 * @param  seq 할일 아이디
	 * @return     완료 처리
	 */
	public Task complete(Long seq) {
		
		Optional<Task> findTask = taskRepository.findById(seq);
		if(findTask.isPresent() == false) { throw new NoDataException(); }
		Task targetTask = findTask.get();
		
		// 참조된 할일이 완료 되었는지 확인
		for(TaskReference ref : targetTask.getReferenceTasks()) {
			Optional<Task> refTask = taskRepository.findById(ref.getRefSeq());
			if(false == refTask.get().isComplete()) {
				throw new UnAuthorizedException();
			}
		}
		
		// 완료 처리
		targetTask.complete();
		
		return targetTask;
	}
	
	/** 
	 * 할일 목록
	 * <br><br>
	 * 
	 * @param  pageable 페이징정보
	 * @return          할일 목록
	 */
	@Transactional(readOnly = true)
	public Page<Task> selectList(Pageable pageable){
		
		Page<Task> list = taskRepository.findAll(pageable);
		if(null == list) { throw new NoDataException(); }
		
		return list;
	}
	
	/**
	 * 할일 조회
	 * <br><br>
	 * 
	 * @param  seq 할일 아이디
	 * @return     할일 조회
	 */
	@Transactional(readOnly = true)
	public Task select(Long seq) {
		
		Optional<Task> findTask = taskRepository.findById(seq);
		if(findTask.isPresent() == false) { throw new NoDataException(); }
		
		return findTask.get();
	}
	
	/**
	 * 마지막 입력 된 할일 조회 - 테스트 용
	 * <br><br>
	 * 
	 * @return 마지막 입력 된 할일 조회
	 */
	@Transactional(readOnly = true)
	public Task selectLastTask() {
		
		Task findTask = taskRepository.findTop1ByOrderBySeqDesc();
		if(findTask == null) { throw new NoDataException(); }
		
		return findTask;
	}
	
	/**
	 * 할일 등록 
	 * <br><br>
	 * 
	 * @param  task 할일 요청 정보
	 * @return      할일 등록
	 */
	public Task insert(TaskForm.InsertForm insertForm) {
		
		String title = insertForm.getTitle();
		
		// 할일 저장
		Task task = new Task(title);
		taskRepository.save(task);
		
		// 참조 할일 저장
		saveReferenceTasks(title, task);
		
		return task;
	}

	/**
	 * 참조 할일 저장
	 * <br><br>
	 * 
	 * @param title 할일
	 * @param task  기본 할일
	 */
	private void saveReferenceTasks(String title, Task task) {
		// 참조 할일 저장
		int[] referenceSeqs = UStringTask.getRefenceSeq(title);
		if(null != referenceSeqs) {
			for(int refSeq : referenceSeqs) {
				taskReferenceService.insert(task.getSeq(), Long.valueOf(refSeq));
			}
		}
	}
	
	/** 
	 * 할일 수정 
	 * <br><br>
	 * 
	 * @param  task 할일 요청 정보
	 * @param  seq  할일 아이디
	 * @return      할일 수정
	 */
	public Task update(TaskForm.UpdateForm updateForm, Long seq) {
		
		Optional<Task> findTask = taskRepository.findById(seq);
		if(findTask.isPresent() == false) { throw new NoDataException(); }
		
		// 할일 수정
		Task task = findTask.get();
		task.modifyTask(updateForm.getTitle());
		
		// 기존 참조 할일 삭제
		taskReferenceService.delete(task.getSeq());
		
		// 참조 할일 저장
		saveReferenceTasks(updateForm.getTitle(), task);
		
		return task;
	}

	public void delete(Long seq) {
		
		// 참조 할일 삭제
		taskReferenceService.delete(seq);
		
		// 할일 삭제
		taskRepository.deleteById(seq);
		
	}
	
	@Autowired private TaskRepository      taskRepository        = null;
	@Autowired private TaskReferenceService taskReferenceService = null;
}
