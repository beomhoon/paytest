package com.kakaopay.task.controller;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kakaopay.common.helper.model.ModelHelper;
import com.kakaopay.task.domain.TaskForm;
import com.kakaopay.task.service.TaskService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author       정범훈
 * @since        2019.01.31
 * @description  REST CONTROLLER
 * @history
 *       
 *  수정일                    수정자                  수정내용
 *  ----------   ---------   -------------------------------
 *  2019.01.31      정범훈                  최초생성	
 *****************************************************************************************************/
@Api(tags="Task APIs", description="할일")
@RestController
public class TaskController {
	
	@ApiOperation(
			value="할일 목록", 
			notes="예) /api/tasks?page=1&size=10&sort=seq,desc&sort=createDate,asc",
			response=TaskForm.ListForm.class
	)
	@GetMapping("/api/tasks")
	public Object findAll(Pageable pageable){

		return ModelHelper.page(taskService.selectList(pageable), TaskForm.ResponseForm.class, pageable);
	}
	
	@ApiOperation(value="할일 조회", response=TaskForm.ResponseForm.class)
	@GetMapping("/api/tasks/{seq}")
	public Object findOne(@PathVariable Long seq){
		
		TaskForm.ResponseForm response = modelMapper.map(taskService.select(seq), TaskForm.ResponseForm.class);
		return response;
	}
	
	@ApiOperation(value="할일 등록", response=TaskForm.ResponseForm.class)
	@PostMapping("/api/tasks")
	public Object insert(@RequestBody TaskForm.InsertForm insertForm, BindingResult bind){
		
		TaskForm.ResponseForm response = modelMapper.map(taskService.insert(insertForm), TaskForm.ResponseForm.class);
		return response;
	}
	
	@ApiOperation(value="할일 수정", response=TaskForm.ResponseForm.class)
	@PutMapping("/api/tasks/{seq}")
	public Object update(@PathVariable Long seq, @Valid @RequestBody TaskForm.UpdateForm updateForm, BindingResult bind){
		
		TaskForm.ResponseForm response = modelMapper.map(taskService.update(updateForm, seq), TaskForm.ResponseForm.class);
		return response;
	}

	@ApiOperation(value="완료 처리", response=TaskForm.ResponseForm.class)
	@GetMapping("/api/tasks/{seq}/complete")
	public Object complete(@PathVariable Long seq){
		
		TaskForm.ResponseForm response = modelMapper.map(taskService.complete(seq), TaskForm.ResponseForm.class);
		return response;
	}
	
//	@ApiOperation(value="할일 삭제", response=TaskForm.ResponseForm.class)
//	@DeleteMapping("/api/tasks/{seq}")
//	public Object delete(@PathVariable Long seq){
//		
//		TaskForm.ResponseForm response = modelMapper.map(taskService.delete(seq), TaskForm.ResponseForm.class);
//		return response;
//	}
	
	@Autowired private ModelMapper modelMapper = null;
	@Autowired private TaskService taskService = null;
}
