package com.kakaopay.task.util;

/**  
 * @author       정범훈
 * @since        2019.01.31
 * @description  문자 유틸
 * @history
 *       
 *  수정일                수정자              수정내용
 *  ----------   ---------   -------------------------------
 *  2019.01.31      정범훈              최초생성	
 *****************************************************************************************************/
public class UStringTask {
		
	/**
	 * 참조 seq 추출
	 * @param title
	 * @return
	 */
	public static int[] getRefenceSeq(String title) {
		
		// @ 카운팅
		int countMentions = 0;
		char titles[] = title.toCharArray();
		for(int i = 0; i < titles.length; i++) {
			if (titles[i] == '@') {
				countMentions++; 
			}
		}
		if(countMentions == 0) {
			return null;
		}
		
		// 참조 seq 추출
		int[] referenceSeqs = new int[countMentions];
		String splitTitle[] = title.split(" ");
		
		int i = 0;
		for(String s : splitTitle) {
			if(0 == s.indexOf('@') && s.length() > 0) {
				referenceSeqs[i] = Integer.parseInt(s.substring(1));
				i++;
			}
		}
		
		return referenceSeqs;
	}
}
