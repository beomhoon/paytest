package com.kakaopay.task.domain;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

public class TaskForm {
	
	@Getter
	@Setter
	@ApiModel(value="TaskForm.ListForm")
	public static class ListForm {
		@ApiModelProperty(notes="아이디")
		private Long   seq        = null;
		@ApiModelProperty(notes="할일")
		private String title      = null;
		@ApiModelProperty(notes="완료여부")
		private boolean isComplete;
		@ApiModelProperty(notes="작성일자")
		private Date   createDate = null;
		@ApiModelProperty(notes="최종수정일자")
		private Date   updateDate = null;
	}
	
	@Getter
	@Setter
	@ApiModel(value="TaskForm.InsertForm")
	public static class InsertForm {
		@ApiModelProperty(notes="할일", required=true, example="방청소 @1 @3")
		private String title = null;
	}
	
	@Getter
	@Setter
	@ApiModel(value="TaskForm.UpdateForm")
	public static class UpdateForm {
		@ApiModelProperty(notes="할일", required=true, example="방청소 @1 @3")
		private String title = null;
	}
	
	@Getter
	@Setter
	@ApiModel(value="TaskForm.ResponseForm")
	public static class ResponseForm {
		@ApiModelProperty(notes="아이디")
		private Long   seq        = null;
		@ApiModelProperty(notes="할일")
		private String title      = null;
		@ApiModelProperty(notes="완료여부")
		private boolean isComplete;
		@ApiModelProperty(notes="작성일자")
		private Date   createDate = null;
		@ApiModelProperty(notes="최종수정일자")
		private Date   updateDate = null;
	}
}
