package com.kakaopay.task.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Entity
@Table(name="tb_task")
public class Task {

	@Id
	@GeneratedValue
	private Long    seq        = null;
	private String  title      = null;
	private boolean isComplete = false;
	@Column(updatable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date    createDate = null;
	@Temporal(TemporalType.TIMESTAMP)
	private Date    updateDate = null;
	
	@OneToMany(mappedBy = "referenceTask", fetch = FetchType.LAZY)
	private List<TaskReference> referenceTasks = new ArrayList<TaskReference>(); 
	
	/**
	 * 등록
	 * @param title
	 */
	public Task(String title) {
		
		this.title      = title;
		Date date       = new Date();
		this.isComplete = false;
		this.createDate = date;
		this.updateDate = date;
	}
	
	/**
	 * 수정
	 * @param title
	 */
	public void modifyTask(String title) {
		
		this.title      = title;
		this.updateDate = new Date();
	}
	
	/**
	 * 완료처리
	 */
	public void complete() {
		
		this.isComplete = true;
	}
}
