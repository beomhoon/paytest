package com.kakaopay.task.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Entity
@Table(name="tb_task_reference")
public class TaskReference {

	@Id
	private Long seq    = null; // 할일 아이디
	private Long refSeq = null; // 참조된 할일 아이디

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "seq", referencedColumnName = "seq", nullable = false, insertable = false, updatable = false)
	private Task referenceTask = null;
	
	/**
	 * 등록
	 * 
	 * @param seq
	 * @param refSeq
	 */
	public TaskReference(Long seq, Long refSeq) {
		this.seq    = seq;
		this.refSeq = refSeq;
	}
}
