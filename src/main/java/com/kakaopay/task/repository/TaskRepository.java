package com.kakaopay.task.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kakaopay.task.domain.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {

	Task findTop1ByOrderBySeqDesc();
}
