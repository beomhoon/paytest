package com.kakaopay.task.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kakaopay.task.domain.TaskReference;

@Repository
public interface TaskReferenceRepository extends JpaRepository<TaskReference, Long> {

	List<TaskReference> findAllBySeq(Long seq);
}
