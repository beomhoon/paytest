### 문제해결 전략
  - 가정
      * 참조는[ @참조번호 ] 로 구성 (양 사이 공백)
  - 데이터베이스 
      * tb_task 에 할일 저장 seq
      * ta_task_reference 에 참조된 항목을 저장하여 완료시 체크 
  - 구현 순서
      * domain : Spring Data JPA 로 구현
      * service 구현 > j-unit 테스트 (/paytest/src/test/java/com/kakaopay/task/TaskTest.java)
      * controller 구현 > swagger-ui 테스트 (http://localhost:8010/swagger-ui.html)
      * UI 구현 > 브라우져 테스트 (http://localhost:8010/task)

### 프로젝트 빌드 방법
  - 프로젝트 선택 후 우클릭 > Maven > Update Project (Alt + F5)

### 실행 방법
  - /paytest/src/main/java/com/kakaopay/PaytestApplication.java 파일선택 후 우클릭 > Run as > Spring Boot App (Alt+Shift+X, B)
  - sql
  
-- drop table tb_task;

create table tb_task (
 seq bigint auto_increment primary key not null,
 title varchar(255),
 isComplete boolean,
 createDate timestamp,
 updateDate timestamp
);

-- drop table tb_task_reference;

create table tb_task_reference (
 seq bigint not null,
 refSeq bigint not null,
);
